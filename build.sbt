import Syntax._
import xerial.sbt.Sonatype.GitLabHosting

val Versions = new {
  val scalatest    = "3.1.0"
  val scalaLogging = "3.9.2"
  val cats         = "2.1.0"
}

val root = project
  .in(file("."))
  .settings(
    name                       := "gnome-scala",
    organization               := "io.gitlab.mateuszjaje",
    idePackagePrefix.invisible := Some("io.gitlab.mateuszjaje.gnome.scala"),
    scalaVersion               := "2.13.3",
    crossScalaVersions         := Seq("2.12.12", "2.13.3"),
    libraryDependencies ++= Seq(
      "io.gitlab.mateuszjaje"       % "java-gtk-maven" % "4.1.3"            % Provided,
      "com.typesafe.scala-logging" %% "scala-logging"  % Versions.scalaLogging,
      "org.typelevel"              %% "cats-core"      % Versions.cats,
      "org.scalatest"              %% "scalatest"      % Versions.scalatest % Test,
    ),
    Test / fork := true,

    //Compile / compile := ((Compile / compile) dependsOn tut).value
    Test / publishArtifact := false,

    // Bintray
    licenses += ("MIT", url("http://opensource.org/licenses/MIT")),

    // Release
    releaseTagComment        := s"Releasing ${(ThisBuild / version).value} [skip ci]",
    releaseCommitMessage     := s"Setting version to ${(ThisBuild / version).value} [skip ci]",
    releaseNextCommitMessage := s"Setting version to ${(ThisBuild / version).value} [skip ci]",
    releaseCrossBuild        := true,
    publishMavenStyle        := true,
    sonatypeProjectHosting   := Some(GitLabHosting("mateuszjaje", "gnome-scala", "mateuszjaje@gmail.com")),
    developers       := List(Developer("mateuszjaje", "Mateusz Jaje", "mateuszjaje@gmail.com", new URL("https://gitlab.com/mateuszjaje"))),
    organizationName := "Mateusz Jaje",
    organizationHomepage   := Some(url("https://gitlab.com/mateuszjaje")),
    versionScheme          := Some("semver-spec"),
    publishTo              := sonatypePublishToBundle.value,
    sonatypeCredentialHost := "s01.oss.sonatype.org",
    releaseProcess := {
      import sbtrelease.ReleaseStateTransformations._
      Seq[ReleaseStep](
        checkSnapshotDependencies,
        inquireVersions,
        runClean,
        runTest,
        setReleaseVersion,
        commitReleaseVersion,
        tagRelease,
        releaseStepCommandAndRemaining("+publishSigned"),
        releaseStepCommand("sonatypeBundleRelease"),
        setNextVersion,
        commitNextVersion,
        pushChanges,
      )
    },
  )

val examples = project
  .in(file("examples"))
  .dependsOn(root)
  .settings(
    scalaVersion    := "2.13.1",
    publishArtifact := false,
  )
